# Lab08 - Rust

Fibonacci and number manipulation (aliquot sum etc)

## Learning objectives for Lab08, as outlined by lecturer

> ### Objectives for Lab08
> Basics of Rust:
> - lists
> - crates
> - recursion

----------------

## Task 1 - Implementation of fibonacci function in Rust

Implementations of 2 different fibonacci functions, `fibonacci_l` and `fibonacci_r`, where:

- `fibonacci_l` uses a loop
- `fibonacci_r` uses recursion

The largest number n that can be calculated using u64 data type is n=264, which returns the fib=18446744073709551615

*Output in main from task 1*
```
======== Lab 08 fibonacci ========
10th Fibonacci number:
lazy:            55
loop:            55
recursive:       55
```


## Task 2 - Aliquot

Implementation of function `aliquot` that calculates the aliquot sum of a number, and then the `classify_number` decides whether it's a `Perfect`, `Deficient` or `Abundant` number.
The aliquot sum of a number is the sum of its divisors (other than the number itself). So, the aliquot sum for 10 would be `1 + 2 + 5 = 8`, and this is `Deficient`

- `Perfect` number - a number that is equal to its aliquot sum
- `Deficient` number - a number that is less than its aliquot sum
- `Abundant` number - a number that is greater than its aliquot sum

#### *Output from main for task 2*

```
======== Lab 08 numbers ========
The number 28 is Perfect
        Calculated in: 500 nano secs
The number 496 is Perfect
        Calculated in: 3800 nano secs
```

The functions return the values wrapped in the Option<> type, in case it returns None instead of an actual value, as a form of error handling in Rust.